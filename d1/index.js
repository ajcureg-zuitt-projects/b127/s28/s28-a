//Javascript is by default is a synchronous. Meaning that only one statement is executed at a time.
/*
console.log("Hello World")
console.log("Hello Again")*/

/*for(let i = 0; i <= 1500; i++){
	console.log(i);
}*/

// console.log("Goodbye");

//When an action will take some time to process, this results in code is called "blocking".

//blocking is when the execution of certain statement JS process must wait until the operation completes.

//Asynchronous means that we can proceed to excute other statments, while time consuming code is running in the background.

//setTimeout() method = to set a time on how long it will take before the next function will appear/effect.

/*console.log("Hello")
setTimeout(function(){
	console.log("John")
}, 2000)

console.log("I am")*/

//the PROMISE object represents the eventual completion (or failure) of an asynchronous operation and its resulting value
//there are 3 possible states: fulfilled, rejected(failed) or pending.

//fetch() is a method in JS, which allows to send a request to an API and process its response.
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*

fetch syntax:
fetch(url, {options}) //url = the url found in our server(routes or endpoint), options = headers(										status,content-type or method)
.then(response => response.json()) //parse the response as JSON
.then(data => {
	console.log(data) 
})//process the result

*/

/*//retrieves all posts following the REST API (retrieve, /posts, GET)
//by using the .then method, we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
//the "fetch" method will return a "promise" that resolves to a "response" object.
//the "then" method captures "response" object and returns another "promise" which will eventually be "resolved" or rejected.
.then(response => response.json())
.then(json => console.log(json))*/

//The 'async' and 'await' keywords is another approach that can be used to achieve asynchronous code.
//used in function to indicate which portions of code should be waited for
//creates an asynchronous function

/*async function fetchData(){

	//waits for the 'fetch' method to complete then stores the value in the 'result' variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	//result returned by fetch is a promise
	console.log(result);
	//the returned 'promise/response' is an object
	console.log(typeof result);
	let json = await result.json();

	console.log(json)
}

fetchData()*/

//Retrieves a specific post (POST)
fetch('https://jsonplaceholder.typicode.com/posts/3')
  .then(response => response.json())
  .then(json => console.log(json))

//create a new post
fetch('https://jsonplaceholder.typicode.com/posts', {
	//sets the method of the "request" object to "post" the ff REST API
	method: 'POST',
	headers: {
		'content-type': 'application/json'
	},
	//sets the content/body data of the 'request' object to be sent to the bakc ent
	//JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})

.then(res => res.json())
.then(data => console.log(data))

//Update a specific post (PUT) = is a method of modifying resource where the client sends data that updates the ENTIRE resources. It is used to set an entity's information completely.
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Updated post',
		body: 'Hello Again'
	})
})

.then(res => res.json())
.then(data => console.log(data))

//(PATCH) = applies a partial update to the resource.
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated post',
	})
})

.then(res => res.json())
.then(data => console.log(data))

//(DELETE) = delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE',
})

.then(res => res.json())
.then(data => console.log(data))

//Filtering posts = the data can be filtered by sending the userId along with the URL
// Syntax
	//Individaual parameters
		//'url?parameterName=value'
	//Multiple Parameters
		//'url?paramA=valueA&paramB=ValueB'

//actual sample of individual filtering
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))

//actual sample of multiple filtering
fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2')
.then(res => res.json())
.then(data => console.log(data))

//the ? symbol at the end of url indicates that parameters will be sent to the endpoint.

//Retrieving nested/related comments to posts
//Retrieve comments for a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))